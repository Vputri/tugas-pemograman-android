package vika.rlapp.model.register;

import com.google.gson.annotations.SerializedName;

public class Register{

	@SerializedName("data")
	private Register_Data registerData;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public void setRegisterData(Register_Data registerData){
		this.registerData = registerData;
	}

	public Register_Data getRegisterData(){
		return registerData;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}
}