package vika.rlapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ornach.nobobutton.NoboButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vika.rlapp.api.ApiClient;
import vika.rlapp.api.ApiInterface;
import vika.rlapp.model.login.Login;
import vika.rlapp.model.register.Register;

public class register extends AppCompatActivity {
    Button btnBackLogin;
    NoboButton btnRegistrasi;
    EditText edtUserName, edtNamaLengkap, edtPassword, edtEmail;
    String name, username, password, email;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        getSupportActionBar().hide();
        btnBackLogin = (Button) findViewById(R.id.btnBackLogin);
        btnRegistrasi = (NoboButton) findViewById(R.id.btnRegistrasi);
        edtUserName = (EditText) findViewById(R.id.edtUserName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtNamaLengkap = (EditText) findViewById(R.id.edtNamaLengkap);

        btnBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(register.this, login.class);
                startActivity(i);
                finish();
            }
        });

        btnRegistrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edtUserName.getText().toString();
                String password = edtPassword.getText().toString();
                String name = edtNamaLengkap.getText().toString();
                String email = edtEmail.getText().toString();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<Register> call = apiInterface.registerResponse(username, name, email, password);
                call.enqueue(new Callback<Register>() {
                    @Override
                    public void onResponse(Call<Register> call, Response<Register> response) {

                        if(response.body() != null && response.isSuccessful() && response.body().isStatus()){
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(register.this, login.class);
                            startActivity(i);
                            finish();
                        }
                        else{
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Register> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}