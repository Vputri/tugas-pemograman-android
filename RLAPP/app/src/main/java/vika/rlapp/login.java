package vika.rlapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ornach.nobobutton.NoboButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vika.rlapp.api.ApiClient;
import vika.rlapp.api.ApiInterface;
import vika.rlapp.model.login.Login;
import vika.rlapp.model.login.Login_Data;

public class login extends AppCompatActivity{

    Button btnRegistrasi;
    NoboButton btnLogin;
    EditText edtUserName, edtPassword;
    ApiInterface apiInterface;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        getSupportActionBar().hide();

        btnRegistrasi = (Button) findViewById(R.id.btnRegistrasi);
        btnLogin      = (NoboButton) findViewById(R.id.loginBtn);
        edtUserName = (EditText) findViewById(R.id.edtUserName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        btnRegistrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(login.this, register.class);
                startActivity(i);
                finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strUsername = edtUserName.getText().toString();
                String strPassword = edtPassword.getText().toString();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<Login> loginCall = apiInterface.loginResponse(strUsername, strPassword);
                loginCall.enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Call<Login> call, Response<Login> response) {
                        if(response.body() != null && response.isSuccessful() && response.body().isStatus()){


                            sessionManager = new SessionManager(getApplicationContext());
                            Login_Data LoginData = response.body().getLoginData();
                            sessionManager.createLogInSession(LoginData);

                            Toast.makeText(getApplicationContext(), response.body().getLoginData().getName(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(login.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        }
                        else{
                            Toast.makeText(login.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Login> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}