package vika.rlapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    SessionManager sessionManager;
    TextView etName;
    String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        sessionManager = new SessionManager(getApplicationContext());
        if(!sessionManager.isLoggedIn()){
            moveToLogin();
        }
        etName = findViewById(R.id.etMainName);
        name = sessionManager.getUserDetail().get(SessionManager.NAME);
        etName.setText(name);
    }

    private void moveToLogin() {
        Intent i = new Intent(MainActivity.this, login.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.actionLogout:
                sessionManager.LogoutSession();
                finish();
                moveToLogin();
        }
        return super.onOptionsItemSelected(item);
    }
}