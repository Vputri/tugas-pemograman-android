package vika.logcat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    int count = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        count++;
        Log.i("My activity", "hasil logcat i");
        Log.v("My activity", "output to logcat");
        Log.d("My activity", "value of count "+count);
    }
}